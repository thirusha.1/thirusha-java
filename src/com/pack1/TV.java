package com.pack1;

public class TV implements TVremote{

	@Override
	public void smart() {
		System.out.println("using smart method");
	}

	@Override
	public void remote() {
		System.out.println("using remote method");
	}
public static void main(String[] args) {
	TV t= new TV();
	t.smart();
	t.remote();
}
}
