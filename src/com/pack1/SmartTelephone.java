package com.pack1;

public class SmartTelephone extends Telephone{

	@Override
	void lift() {
		System.out.println("using Lift method");
	}

	@Override
	void disconnected() {
		System.out.println("using disconnected method");
	}
public static void main(String[] args) {
	SmartTelephone s= new SmartTelephone();
	s.lift();
	s.disconnected();
}
}
