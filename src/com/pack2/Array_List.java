package com.pack2;
import java.util.ArrayList;

public class Array_List {
	public static void main(String[] args) {
		ArrayList<String> l = new ArrayList<>();
		l.add("Apple");
		l.add("Orange");
		l.add("Coke");
		l.add("Mango");
		l.add("Watermelon");
		System.out.println(l.size());
		System.out.println(l);
		l.remove(1);
		if (l.contains("Coke")) {
			System.out.println("Coke is present in list");

		} else {
			System.out.println("Coke is not present in list");

		}

	}
}
